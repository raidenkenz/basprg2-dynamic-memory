#pragma once

#include <iostream>
#include <string>

using namespace std;

class Character
{
public:
	Character();
	Character(string name, int hp, int mp, string weaponName, int weaponDamage);
	void displayStats();
	void attack(Character* enemy);
	bool dead();
private:
	string name;
	int hp;
	int mp;
	string weaponName;
	int weaponDamage;
};