#include <iostream>
#include <string>
#include <time.h>
#include "Character.h"
#include <vector>

using namespace std;

int main()
{
	srand(time(NULL));
	int Choice = 0;
	vector<Character*> playerClass;
	vector<Character*> enemy;

	cout << " ========= Welcome to RPG! =========" << endl;
	system("cls");

	{
		Character *enemyPlayer = new Character("Demon King", 100, 75, "Destruction Sword", 8);
		enemy.push_back(enemyPlayer);
	}

	cout << "Hello. I am your guide." << endl;
	system("cls");
	cout << "Choose your class you like to be..." << endl;
	cout << endl;
	cout << "1 - Paladin" << endl;
	cout << "2 - Ranger" << endl;
	cout << "3 - Wizard" << endl;
	cout << "4 - Knight" << endl;
	cout << "5 - Rogue" << endl;

	cout << endl;
	cout << "Input your chosen class: ";
	cin >> Choice;

	cout << endl;

	if (Choice == 1)
	{
		Character *Paladin = new Character("Paladin", 130, 70, "Sword and Shield", 10);
		playerClass.push_back(Paladin);
		cout << "You chose the Paladin class!" << endl;
	}

	else if (Choice == 2)
	{
		Character *Ranger = new Character("Ranger", 160, 70, "Bow and Arrows", 8);
		playerClass.push_back(Ranger);
		cout << "You chose the Ranger class!" << endl;
		
	}

	else if (Choice == 3)
	{
		Character *Wizard = new Character("Wizard", 110, 90, "Fire ball", 9);
		playerClass.push_back(Wizard);
		cout << "You chose the Wizard class!" << endl;
	}

	else if (Choice == 4)
	{
		Character *Warrior = new Character("Warrior", 150, 75, "Long Sword", 7);
		playerClass.push_back(Warrior);
		cout << "You chose the Warrior class!" << endl;
	}

	else if (Choice == 5)
	{
		Character *Rogue = new Character("Rogue", 120, 90, "Dagger", 8);
		playerClass.push_back(Rogue);
		cout << "You chose the Rogue class!" << endl;
	}

	system("pause");
	system("cls");

	cout << "The Battle is already starting!" << endl;
	system("pause");
	system("cls");

	while (true)
	{
		int skillChance = rand() % 100 + 1;
		int i = 0;
		playerClass[i]->displayStats();
		cout << endl;
		enemy[i]->displayStats();

		system("pause");
		system("cls");
		while (true)
		{
			int i = 0;
			playerClass[i]->attack(enemy[i]);
			cout << endl;
			enemy[i]->attack(playerClass[i]);
			cout << endl;
			system("cls");

			playerClass[i]->displayStats();
			cout << endl;
			enemy[i]->displayStats();
			system("pause");
			system("cls");

			if (playerClass[i]->dead())
			{
				cout << "The Demon King killed you on death battle." << endl;
				cout << "He took a over world and rule around the world..." << endl;
				system("pause");
				system("cls");
				break;
			}

			else if (enemy[i]->dead())
			{
				cout << "You won against Demon King in death battle!" << endl;
				cout << "You saved the world from Demon King!" << endl;
				system("pause");
				system("cls");
				break;
			}

			else if (enemy[i]->dead() && playerClass[i]->dead())
			{
				cout << "The Legend lost his life to save the world from Demon King's donimation!" << endl;
				cout << "He defeated the Demon King!" << endl;
				cout << "Everyone honors his death... " << endl;
				system("pause");
				system("cls");
				break;
			}
		}

		break;
	}

	cout << "======== THANK YOU FOR PLAYING THIS GAME!! ========" << endl;
	system("pause");
}